package net.rafkos.tools.RomeTWSaveEditor;

public enum UnitSize
{
	SMALL, NORMAL, LARGE, HUGE;
}
