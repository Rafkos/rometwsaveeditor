package net.rafkos.tools.RomeTWSaveEditor;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.border.EtchedBorder;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JSpinner;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

public class App
{
	private JFrame frame;
	private static final int WIDTH = 394, HEIGHT = 449;
	private static final String VERSION = "0.0.1-SNAPSHOT";
	private SaveFile saveFile;
	private JComboBox<Difficulty> campaignDifficultyCombo;
	private JComboBox<Difficulty> battleDifficultyCombo;
	private JComboBox<UnitSize> unitSizeCombo;
	private JSpinner yearSpinner;
	private JComboBox<Season> seasonCombo;
	private JCheckBox battleTimeLimitCheckBox;
	private JComboBox<BinaryPointers> gameVersionCombo;
	private JPanel optionsPanel;
	private JButton btnSaveAs;
	private JTextField lblSavePath;
	private JLabel campaignName;
	private JLabel dataFolderName;
	private File lastSelectedDir = new File("");
	private JLabel authorLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{

		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1)
		{
			e1.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					App window = new App();
					window.frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public App()
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frame = new JFrame("Rome TW - Save Editor "+VERSION);
		frame.setResizable(false);
		frame.setBounds(100, 100, 100, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		lblSavePath = new JTextField("");
		lblSavePath.setEditable(false);
		lblSavePath.setBounds(12, 77, 283, 26);
		frame.getContentPane().add(lblSavePath);
		lblSavePath.setBorder(BorderFactory.createBevelBorder(1));

		JButton btnChooseSave = new JButton("Open");
		btnChooseSave.setBounds(300, 77, 80, 26);
		frame.getContentPane().add(btnChooseSave);

		btnSaveAs = new JButton("Save as");
		btnSaveAs.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				saveFile();
			}
		});
		btnSaveAs.setBounds(300, 109, 79, 26);
		frame.getContentPane().add(btnSaveAs);

		optionsPanel = new JPanel();
		optionsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Options",
				TitledBorder.LEFT, TitledBorder.TOP, null, null));
		optionsPanel.setBounds(12, 163, 368, 271);
		frame.getContentPane().add(optionsPanel);
		optionsPanel.setLayout(null);

		JLabel lblCampaignDifficulty = new JLabel("Campaign difficulty");
		lblCampaignDifficulty.setBounds(12, 30, 154, 16);
		optionsPanel.add(lblCampaignDifficulty);

		campaignDifficultyCombo = new JComboBox<Difficulty>();
		campaignDifficultyCombo.setBounds(12, 58, 174, 25);
		optionsPanel.add(campaignDifficultyCombo);

		JLabel lblBattleDifficulty = new JLabel("Battle difficulty");
		lblBattleDifficulty.setBounds(12, 95, 154, 16);
		optionsPanel.add(lblBattleDifficulty);

		battleDifficultyCombo = new JComboBox<Difficulty>();
		battleDifficultyCombo.setBounds(12, 123, 174, 25);
		optionsPanel.add(battleDifficultyCombo);

		unitSizeCombo = new JComboBox<UnitSize>();
		unitSizeCombo.setBounds(12, 188, 174, 25);
		optionsPanel.add(unitSizeCombo);

		JLabel lblUnitSize = new JLabel("Unit size");
		lblUnitSize.setBounds(12, 160, 154, 16);
		optionsPanel.add(lblUnitSize);

		yearSpinner = new JSpinner();
		yearSpinner.setBounds(221, 58, 135, 25);
		yearSpinner.setModel(new SpinnerNumberModel(0, -50000, 50000, 1));
		optionsPanel.add(yearSpinner);

		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(221, 30, 135, 16);
		optionsPanel.add(lblYear);

		JLabel lblSeason = new JLabel("Season");
		lblSeason.setBounds(221, 95, 135, 16);
		optionsPanel.add(lblSeason);

		seasonCombo = new JComboBox<Season>();
		seasonCombo.setBounds(221, 123, 135, 25);
		optionsPanel.add(seasonCombo);

		battleTimeLimitCheckBox = new JCheckBox("Battle time limit");
		battleTimeLimitCheckBox.setBounds(8, 229, 178, 24);
		optionsPanel.add(battleTimeLimitCheckBox);
		
		authorLabel = new JLabel("by Rafkos, admin@rafkos.net");
		authorLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		authorLabel.setBounds(178, 234, 178, 16);
		optionsPanel.add(authorLabel);

		gameVersionCombo = new JComboBox<BinaryPointers>();
		gameVersionCombo.setBounds(12, 40, 368, 25);
		frame.getContentPane().add(gameVersionCombo);

		JLabel lblGameVersion = new JLabel("Game version");
		lblGameVersion.setBounds(12, 12, 135, 16);
		frame.getContentPane().add(lblGameVersion);
		
		campaignName = new JLabel("");
		campaignName.setBounds(12, 114, 278, 16);
		frame.getContentPane().add(campaignName);
		
		dataFolderName = new JLabel("");
		dataFolderName.setBounds(12, 136, 278, 16);
		frame.getContentPane().add(dataFolderName);
		btnChooseSave.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				openFile();
			}
		});
		frame.setVisible(true);

		recalculateSize();

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);

		loadConfig();
		lockProgram();
	}

	@SuppressWarnings(
	{ "unchecked", "rawtypes" })
	private void loadConfig()
	{
		BinaryPointersLoader bpl = new BinaryPointersLoader();
		bpl.loadAll();
		gameVersionCombo.setModel(new DefaultComboBoxModel(bpl.getPointers().toArray()));
		campaignDifficultyCombo.setModel(new DefaultComboBoxModel<Difficulty>(Difficulty.values()));
		battleDifficultyCombo.setModel(new DefaultComboBoxModel<Difficulty>(Difficulty.values()));
		unitSizeCombo.setModel(new DefaultComboBoxModel<UnitSize>(UnitSize.values()));
		seasonCombo.setModel(new DefaultComboBoxModel<Season>(Season.values()));
	}

	private void updateSaveInfoLabel()
	{
		campaignName.setText("Campaign name: "+saveFile.getCampaignName());
		dataFolderName.setText("Data folder name: "+saveFile.getDataFolderName());
	}

	private void recalculateSize()
	{
		int heightOff = frame.getHeight() - frame.getContentPane().getHeight();
		int widthOff = frame.getWidth() - frame.getContentPane().getWidth();
		frame.setSize(WIDTH + widthOff, HEIGHT + heightOff);
		frame.repaint();
	}

	protected void openFile()
	{
		final JFileChooser fc = new JFileChooser(lastSelectedDir);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("RomeTW Save", ".sav", "sav");
		fc.setFileFilter(filter);
		int returnVal = fc.showOpenDialog(frame);

		if (returnVal == JFileChooser.APPROVE_OPTION)
		{
			File file = fc.getSelectedFile();
			lastSelectedDir = file;
			loadSaveFile(file);
		}
	}

	protected void saveFile()
	{
		final JFileChooser fc = new JFileChooser(lastSelectedDir);
		int returnVal = fc.showSaveDialog(frame);

		if (returnVal == JFileChooser.APPROVE_OPTION)
		{
			File file = fc.getSelectedFile();
			lastSelectedDir = file;
			String path = file.getPath();
			if (!path.endsWith(".sav"))
			{
				path += ".sav";
			}
			saveSaveFile(new File(path));
		}
	}

	private void saveSaveFile(File file)
	{
		try
		{
			saveFile.setBattleDifficulty((Difficulty) battleDifficultyCombo.getSelectedItem());
			saveFile.setCampaignDifficulty((Difficulty) campaignDifficultyCombo.getSelectedItem());
			saveFile.setUnitSize((UnitSize) unitSizeCombo.getSelectedItem());
			saveFile.setSeason((Season) seasonCombo.getSelectedItem());
			yearSpinner.commitEdit();
			saveFile.setYear((int) yearSpinner.getValue());
			BattleTimeLimit timeLimit = battleTimeLimitCheckBox.isSelected() == true ? BattleTimeLimit.BATTLE_TIME_LIMIT
					: BattleTimeLimit.NO_BATTLE_TIME_LIMIT;
			saveFile.setBattleTimeLimit(timeLimit);
		} catch (Exception e)
		{
			message("An unspecified error has occured.", false);
			e.printStackTrace();
			return;
		}
		try
		{
			saveFile.getBinaryFile().saveFile(file);
			message("File saved.", true);
		} catch (IOException e)
		{
			message("Unable to save file in this location.", false);
			e.printStackTrace();
		}
	}

	private void loadSaveFile(File file)
	{
		lblSavePath.setText("");
		if (gameVersionCombo.getSelectedItem() == null)
		{
			message("Game version not selected.", false);
		} else
		{
			BinaryFile bf = null;
			try
			{
				bf = new BinaryFile(file);
				SaveFile newSaveFile = new SaveFile(bf, (BinaryPointers) gameVersionCombo.getSelectedItem());
				// test
				if (newSaveFile.isSaveFileCorrect())
				{
					saveFile = newSaveFile;
					try
					{
						campaignDifficultyCombo.setSelectedItem(saveFile.getCampaignDifficulty());
						battleDifficultyCombo.setSelectedItem(saveFile.getBattleDifficulty());
						unitSizeCombo.setSelectedItem(saveFile.getUnitSize());
						seasonCombo.setSelectedItem(saveFile.getSeason());
						yearSpinner.setValue(saveFile.getYear());
						boolean timeLimit = saveFile.getBattleTimeLimit() == BattleTimeLimit.BATTLE_TIME_LIMIT ? true
								: false;
						battleTimeLimitCheckBox.setSelected(timeLimit);
					} catch (Exception e)
					{
						// never happens
					}
					lblSavePath.setText(file.getPath());
					message("Save file loaded correctly.", true);
					updateSaveInfoLabel();
					unlockProgram();
				} else
				{
					message("Incompatible save file. Please check version of your game.", false);
					lockProgram();
				}
			} catch (IOException e)
			{
				e.printStackTrace();
				message("Unable to open selected save file.", false);
				lockProgram();
			}

		}
	}

	public void message(String msg, boolean success)
	{
		int type = JOptionPane.ERROR_MESSAGE;
		if (success)
		{
			type = JOptionPane.INFORMATION_MESSAGE;
		}

		JOptionPane.showMessageDialog(frame, msg, "Info", type);
	}

	public void lockProgram()
	{
		for (Component c : optionsPanel.getComponents())
		{
			c.setEnabled(false);
		}
		btnSaveAs.setEnabled(false);
	}

	public void unlockProgram()
	{
		for (Component c : optionsPanel.getComponents())
		{
			c.setEnabled(true);
		}
		btnSaveAs.setEnabled(true);
	}
}
