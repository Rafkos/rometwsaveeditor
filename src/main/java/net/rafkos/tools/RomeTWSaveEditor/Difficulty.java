package net.rafkos.tools.RomeTWSaveEditor;

public enum Difficulty
{
	LOW, MEDIUM, HIGH, VERY_HIGH;
}
