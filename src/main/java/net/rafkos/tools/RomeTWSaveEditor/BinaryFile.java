package net.rafkos.tools.RomeTWSaveEditor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class BinaryFile
{
	private byte[] bytes;

	public BinaryFile(File source) throws IOException
	{
		collectBytes(source);
	}

	private void collectBytes(File source) throws IOException
	{
		bytes = Files.readAllBytes(source.toPath());
	}
	
	public byte[] getBytes()
	{
		return bytes;
	}
	
	public void setBytes(byte[] b)
	{
		bytes = b;
	}
	
	public byte[] getBytesAt(int offset, int len) throws Exception
	{
		if(offset+len > bytes.length)
		{
			throw new Exception("Binary file exceeded.");
		}
		byte[] result = new byte[len];
		for(int i = 0; i < len; i++)
		{
			result[i] = bytes[offset+i];
		}
		return result;
	}
	
	public void setBytesAt(int offset, byte[] newBytes) throws Exception
	{
		if(offset+newBytes.length > bytes.length)
		{
			throw new Exception("Binary file exceeded.");
		}
		for(int i = 0; i < newBytes.length; i++)
		{
			bytes[i+offset] = newBytes[i]; 
		}
	}
	
	public void setByteAt(int offset, byte newByte) throws Exception
	{
		if(offset > bytes.length-1)
		{
			throw new Exception("Binary file exceeded.");
		}
		bytes[offset] = newByte;
	}
	
	public byte getByte(int offset)
	{
		return bytes[offset];
	}
	
	public void saveFile(File dest) throws IOException
	{
		Files.write(dest.toPath(), bytes);
	}
	
}
