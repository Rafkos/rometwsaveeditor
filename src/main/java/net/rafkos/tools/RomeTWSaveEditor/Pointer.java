package net.rafkos.tools.RomeTWSaveEditor;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor @Setter
public class Pointer
{
	@Getter
	private ReferencePoint referencePoint;
	private int[] pointers;
	
	public int[] getPointers(int[] referencePointsValues)
	{
		if(referencePoint == ReferencePoint.DATA_FOLDER)
		{
			return getPointersWithOffset(pointers, referencePointsValues[0]);
		}
		else
		{
			return getPointersWithOffset(pointers, referencePointsValues[1]);
		}
	}

	private int[] getPointersWithOffset(int[] parr, int val)
	{
		int[] newPointers = new int[parr.length];
		for(int i = 0; i < parr.length; i++)
		{
			newPointers[i] = parr[i] + val;
		}
		return newPointers;
	}
}
