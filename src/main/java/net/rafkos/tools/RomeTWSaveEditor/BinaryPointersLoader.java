package net.rafkos.tools.RomeTWSaveEditor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import lombok.Getter;

public class BinaryPointersLoader
{
	private static final Gson gson = new Gson();
	@Getter
	private ArrayList<BinaryPointers> pointers = new ArrayList<>();
	
	public void loadAll()
	{
		pointers = new ArrayList<>();
		ArrayList<File> pointersFiles = collectPointerFiles();
		for(File f : pointersFiles)
		{
			BinaryPointers bp = loadPointer(f);
			if(bp != null)
			{
				pointers.add(bp);
			}
		}
	}

	private BinaryPointers loadPointer(File f)
	{
		BinaryPointers bp = null;
		try
		{
			bp = gson.fromJson(new FileReader(f), BinaryPointers.class);
		} catch (JsonSyntaxException | JsonIOException | FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return bp;
	}

	private ArrayList<File> collectPointerFiles()
	{
		File resourcesDir = new File("pointers/");
		ArrayList<File> files = new ArrayList<>();
		if(resourcesDir.isDirectory())
		{
			for(File content : resourcesDir.listFiles())
			{
				if(content.isFile() && content.getName().endsWith(".json"))
				{
					files.add(content);
				}
			}
		}
		return files;
	}


	
}
