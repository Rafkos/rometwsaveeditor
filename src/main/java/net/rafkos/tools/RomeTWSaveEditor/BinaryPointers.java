package net.rafkos.tools.RomeTWSaveEditor;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class BinaryPointers
{
	private String name;
	private Pointer battleDifficultyPointer;
	private Pointer campaignDifficultyPointer;
	private Pointer battleTimeLimitPointer;
	private Pointer unitSizePointer;
	private Pointer yearPointer;
	private Pointer seasonPointer;

	public String toString()
	{
		return name;
	}
}
