package net.rafkos.tools.RomeTWSaveEditor;

import java.nio.ByteBuffer;

public class SaveFile
{
	
	private BinaryFile binary;
	private BinaryPointers pointers;
	
	private int[] referencePointsValues = new int[]{0, 0};
	private String campaignName = "", dataFolderName = "";
	
	public SaveFile(BinaryFile bin, BinaryPointers point)
	{
		binary = bin;
		pointers = point;
		calculateReferencePoints();
	}
	
	private void calculateReferencePoints()
	{
		int lengthOfDataFolderName = binary.getByte(2); // default position
		byte[] name;
		try
		{
			name = convertToTextArray(binary.getBytesAt(4, lengthOfDataFolderName*2));
			dataFolderName = new String(name);
		} catch (Exception e)
		{
			dataFolderName = "";
			e.printStackTrace();
		}
		referencePointsValues[0] = lengthOfDataFolderName * 2 + 4; 
		int offset = 50; // header offset, should be equal to 500 in all rome total war engine games
		int lengthOfCampaignName = binary.getByte(referencePointsValues[0] + offset);
		try
		{
			name = convertToTextArray(binary.getBytesAt(referencePointsValues[0] + offset + 2, lengthOfCampaignName*2));
			campaignName = new String(name);
		} catch (Exception e)
		{
			campaignName = "";
			e.printStackTrace();
		}
		referencePointsValues[1] = referencePointsValues[0] + offset + lengthOfCampaignName * 2 + 2;
	}

	private byte[] convertToTextArray(byte[] bytes)
	{
		byte[] name = new byte[bytes.length/2];
		for(int i = 0; i < bytes.length; i = i + 2)
		{
			name[i/2] = bytes[i];
		}
		return name;
	}
	
	public BinaryFile getBinaryFile()
	{
		return binary;
	}
	
	public Difficulty getBattleDifficulty() throws Exception
	{
		switch(binary.getByte(pointers.getBattleDifficultyPointer().getPointers(referencePointsValues)[0]))
		{
		case 0x00:
			return Difficulty.LOW;
		case 0x01:
			return Difficulty.MEDIUM;
		case 0x02:
			return Difficulty.HIGH;
		case 0x03:
			return Difficulty.VERY_HIGH;
		}
		throw new Exception();
	}
	
	public void setBattleDifficulty(Difficulty diff) throws Exception
	{
		byte b = 0x00;
		switch(diff)
		{
		case LOW:
			b = 0x00;
			break;
		case MEDIUM:
			b = 0x01;
			break;
		case HIGH:
			b = 0x02;
			break;
		case VERY_HIGH:
			b = 0x03;
			break;
		}
		for(int offset : pointers.getBattleDifficultyPointer().getPointers(referencePointsValues))
		{
			binary.setByteAt(offset, b);
		}
	}
	
	public Difficulty getCampaignDifficulty() throws Exception
	{
		switch(binary.getByte(pointers.getCampaignDifficultyPointer().getPointers(referencePointsValues)[0]))
		{
		case 0x00:
			return Difficulty.LOW;
		case 0x01:
			return Difficulty.MEDIUM;
		case 0x02:
			return Difficulty.HIGH;
		case 0x03:
			return Difficulty.VERY_HIGH;
		}
		throw new Exception();
	}
	
	public void setCampaignDifficulty(Difficulty diff) throws Exception
	{
		byte b = 0x00;
		switch(diff)
		{
		case LOW:
			b = 0x00;
			break;
		case MEDIUM:
			b = 0x01;
			break;
		case HIGH:
			b = 0x02;
			break;
		case VERY_HIGH:
			b = 0x03;
			break;
		}
		for(int offset : pointers.getCampaignDifficultyPointer().getPointers(referencePointsValues))
		{
			binary.setByteAt(offset, b);
		}
	}
	
	public BattleTimeLimit getBattleTimeLimit() throws Exception
	{
		byte b = binary.getByte(pointers.getBattleTimeLimitPointer().getPointers(referencePointsValues)[0]);
		switch(b)
		{
		case 0x1E:
			return BattleTimeLimit.BATTLE_TIME_LIMIT;
		case 0x1C:
			return BattleTimeLimit.NO_BATTLE_TIME_LIMIT;
		}
		throw new Exception();
	}
	
	public void setBattleTimeLimit(BattleTimeLimit bl) throws Exception
	{
		byte b = 0x00;
		switch(bl)
		{
		case BATTLE_TIME_LIMIT:
			b = 0x1E;
			break;
		case NO_BATTLE_TIME_LIMIT:
			b = 0x1C;
			break;
		}
		for(int offset : pointers.getBattleTimeLimitPointer().getPointers(referencePointsValues))
		{
			binary.setByteAt(offset, b);
		}
	}
	
	public UnitSize getUnitSize() throws Exception
	{
		byte[] bytes = binary.getBytesAt(pointers.getUnitSizePointer().getPointers(referencePointsValues)[0], 2);
		if(bytes[0] == 0x00 && bytes[1] == 0x3F)
		{
			return UnitSize.SMALL;
		}
		else if(bytes[0] == -128 && bytes[1] == 0x3F)
		{
			return UnitSize.NORMAL;
		}
		else if(bytes[0] == 0x00 && bytes[1] == 0x40)
		{
			return UnitSize.LARGE;
		}
		else if(bytes[0] == -128 && bytes[1] == 0x40)
		{
			return UnitSize.HUGE;
		}
		throw new Exception();
	}
	
	public void setUnitSize(UnitSize us) throws Exception
	{
		byte[] bytes = new byte[2];
		switch(us)
		{
		case SMALL:
			bytes = new byte[] {0x00, 0x3F};
			break;
		case NORMAL:
			bytes = new byte[] {-128, 0x3F};
			break;
		case LARGE:
			bytes = new byte[] {0x00, 0x40};
			break;
		case HUGE:
			bytes = new byte[] {-128, 0x40};
			break;
		}
		for(int offset : pointers.getUnitSizePointer().getPointers(referencePointsValues))
		{
			binary.setBytesAt(offset, bytes);
		}
	}
	
	public int getYear() throws Exception
	{
		byte[] data = binary.getBytesAt(pointers.getYearPointer().getPointers(referencePointsValues)[0], 4);
		int value = ((data[0]&0xff) |
			    ((data[1]&0xff)<<8) |
			    ((data[2]&0xff)<<16) |
			    ((data[3]&0xff)<<24));
		return value;
	}
	
	public void setYear(int year) throws Exception
	{
		byte[] bytes = ByteBuffer.allocate(4).putInt(year).array();
		//invert
		byte[] inverted = new byte[4];
		for(int i = 3; i >= 0; i--)
		{
			inverted[i] = bytes[3-i];
		}
		for(int offset : pointers.getYearPointer().getPointers(referencePointsValues))
		{
			binary.setBytesAt(offset, inverted);
		}
	}
	
	public Season getSeason() throws Exception
	{
		byte b = binary.getByte(pointers.getSeasonPointer().getPointers(referencePointsValues)[0]);
		switch(b)
		{
		case 0x02:
			return Season.SUMMER;
		case 0x00:
			return Season.WINTER;
		}
		throw new Exception();
	}
	
	public void setSeason(Season s) throws Exception
	{
		byte b = 0x00;
		switch(s)
		{
		case SUMMER:
			b = 0x02;
			break;
		case WINTER:
			b = 0x00;
			break;
		}
		for(int offset : pointers.getSeasonPointer().getPointers(referencePointsValues))
		{
			binary.setByteAt(offset, b);
		}
	}
	
	public String getCampaignName()
	{
		return campaignName;
	}
	
	public String getDataFolderName()
	{
		return dataFolderName;
	}
	
	public boolean isSaveFileCorrect()
	{
		try
		{
			getBattleDifficulty();
			getCampaignDifficulty();
			getBattleTimeLimit();
			getSeason();
			getUnitSize();
			getYear();
		} catch (Exception e)
		{
			return false;
		}
		return true;
	}
	
}
