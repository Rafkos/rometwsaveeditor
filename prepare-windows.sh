#!/bin/bash
rm -rf compiled
mkdir compiled
cd compiled
mkdir RomeTWSaveEditor
cd RomeTWSaveEditor
mkdir pointers/
cd ..
cd ..
cp launch4j/output/rometw-editor.exe compiled/RomeTWSaveEditor/rometw-editor.exe
cp target/RomeTWSaveEditor-0.0.1-SNAPSHOT.jar compiled/RomeTWSaveEditor/
mv compiled/RomeTWSaveEditor/RomeTWSaveEditor-0.0.1-SNAPSHOT.jar compiled/RomeTWSaveEditor/data.jar
cp LICENSE compiled/RomeTWSaveEditor/LICENSE
cp README compiled/RomeTWSaveEditor/README
cp -R pointers/ compiled/RomeTWSaveEditor/